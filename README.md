# What it can be used for :

This JAR executable can be used for two case :
*   Comparing a gold trace to your build trace for the Jenkins grader of Task 5.
*   Visualising a display from a modelsim simulation

**Reminder :** 
* This does not replace ModelSim it is only to visualise the context of an error.
Remind yourself also that the submissions are limited and should not be a way to
test your VHDL code.
(Computer Architecture labs only have 3 or less submissions)

* There is no guarantee of a perfect execution

# How to use it :


## ModelSim test :

1. Append those lines to your script(.do) that you want to simulate after the command `run`.
    Don't forget to replace <TESTED_ENTITY_NAME>, <PATH_TO_LIST.LST> with your values.
    Example : C:/Users/matt/Documents/EPFL_IC/BA2/DSD/TP89/breakout-simulation/resources/list.lst
    For those on Windows the path must be given with "/" and not "\\".



    onerror {resume} \
    add list /<TESTED_ENTITY_NAME>/led_array \
    configure list -usestrobe 0 \
    configure list -strobestart {0 ps} -strobeperiod {0 ps} \
    configure list -usesignaltrigger 1 \
    configure list -delta all \
    configure list -signalnamewidth 0 \
    configure list -datasetprefix 0 \
    configure list -namelimit 5 \
    write list -window .main_pane.list.interior.cs.body <PATH_TO_LIST.LST>

2. Run the modelsim script and check that the list.lst in the resource folder is there


## Jenkins traces test :

Place the unzipped build and gold trace folders in the resource folder, you will then be able to see the traces of your task 5.


## Running the program
Open a terminal and navigate to the directory where the dsd-decoder is located, then run the following command : 

    java --module-path <PATH_TO_FX_LIB> --add-modules javafx.controls -jar DSD_Decoder.jar

Where <PATH_TO_FX_LIB> should be replaced by the path to your java fx `/lib` folder

There you go! You should be able to change frame with either the arrow keys or the top side buttons



# Demonstration :

Here's how the program looks when used to visualize your  output: 

![](resources/demo/ModelSim_Sim_example.gif)
![](resources/demo/jenkins-demo.gif)

#### Special thanks to Sean Perazzolo for making the .lst parser and modelsim script to append
