package gui;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import utils.Parser;

import java.io.IOException;
import java.util.List;

final class ModelSimScene {

    private final int gridWidth, gridHeight;

    private final List<boolean[][]> arrays;

    private final TimeBar timebar;

    private final ObjectProperty<boolean[][]> modelSimTab = new SimpleObjectProperty<>();

    private final AnchorPane pane = new AnchorPane();

    public ModelSimScene(int grid_width, int grid_height, String pathToTrace) throws IOException {
        gridWidth = grid_width;
        gridHeight = grid_height;

        Parser gridParser = new Parser(grid_width, grid_height);
        arrays = gridParser.parseModelsimTest(pathToTrace);

        timebar = new TimeBar(arrays.size() - 1);
        timebar.prefWidthProperty().bind(pane.widthProperty());

        modelSimTab.bind(timebar.index().map(id -> arrays.get(id.intValue())));

        setPane();
    }

    private void setPane() {

        Pane modelSimPane = new Pane();
        pane.getChildren().add(modelSimPane);
        AnchorPane.setBottomAnchor(modelSimPane, 0d);

        modelSimPane.setPrefWidth(App.APP_WIDTH);
        modelSimPane.setPrefHeight(App.APP_HEIGHT);
        modelSimPane.setStyle("-fx-background-color: #d3d3d3");

        pane.getChildren().add(timebar);
        AnchorPane.setTopAnchor(timebar, 5d);

        setGridGroup(modelSimPane);

    }

    private void setGridGroup(Pane modelSimPane) {
        for(int i = 0; i < gridWidth; i++) {
            for (int j = 0; j < gridHeight; j++) {
                final int finalI = i; // Need values final for lambda
                final int finalJ = j;

                Rectangle modelsimRect = new Rectangle(22*i + 300, 22*j + 100, 20, 20);

                modelsimRect.visibleProperty().bind(modelSimTab.map(modelTab -> modelTab != null && modelTab[finalI][finalJ]));
                modelsimRect.setFill(Color.BLACK);
                modelsimRect.setArcWidth(15);
                modelsimRect.setArcHeight(15);

                modelSimPane.getChildren().add(modelsimRect);

            }
        }

    }

    TimeBar getTimebar() {return timebar;}

    Pane getPane() { return pane; }

}
