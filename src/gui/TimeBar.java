package gui;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;


/**
 * Gestion de l'index dans la liste d'arrays
 */
final class TimeBar extends StackPane {

    private final IntegerProperty index = new SimpleIntegerProperty(0);

    private final int maxCount;

    TimeBar(int maxCount) {
        this.maxCount = maxCount;

        Text left = new Text("<<");
        this.getChildren().add(left);
        StackPane.setAlignment(left, Pos.CENTER_LEFT);

        left.setOnMouseClicked(e -> previous());

        Text right = new Text(">>");
        this.getChildren().add(right);
        StackPane.setAlignment(right, Pos.CENTER_RIGHT);

        right.setOnMouseClicked(e -> next());

        Text center = new Text();
        center.textProperty().bind(index.map(id -> "Frame index : " + id));
        this.getChildren().add(center);
        StackPane.setAlignment(center, Pos.CENTER);

        this.setStyle("-fx-font-weight: bold; -fx-font-size: 20px");

        this.setPrefHeight(25);

    }

    /**
     * Getter pour l'index actuel
     * @return (ReadOnlyIntegerProperty) Propriété de l'index
     */
    ReadOnlyIntegerProperty index() { return index; }


    void next() { index.set(Math.max(0, Math.min(maxCount, index.get() + 1))); }
    void previous() { index.set(Math.max(0, Math.min(maxCount, index.get() - 1))); }

}
