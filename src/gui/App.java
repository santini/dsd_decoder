package gui;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.IOException;


public class App extends Application {

    private static final int GRID_HEIGHT = 9;
    private static final int GRID_WIDTH = 12;

    static final int APP_HEIGHT = 420;
    static final int APP_WIDTH = 800;


    private static final String J_STR = "To Jenkins-sim";
    private static final String M_STR = "To Modelsim-sim";


    private final Pane jenkinsPane;
    private final Pane modelsimPane;

    private final JenkinsScene jenkinsScene;
    private final ModelSimScene modelSimScene;



    public App() {
        ModelSimScene modelSimScene1;
        JenkinsScene jenkinsScene1;
        Pane modelsimPane1;
        Pane jenkinsPane1;
        try {
            jenkinsScene1 = new JenkinsScene(GRID_WIDTH, GRID_HEIGHT,
                    "resources/gold/breakout_tb-output",
                    "resources/build/breakout_tb-output");

            jenkinsPane1 = jenkinsScene1.getPane();

        } catch (IOException e) {
            jenkinsPane1 = defaultPane();
            jenkinsScene1 = null;
            System.out.println("Jenkins traces not found");
        }
        jenkinsScene = jenkinsScene1;
        jenkinsPane = jenkinsPane1;

        try {
            modelSimScene1 = new ModelSimScene(GRID_WIDTH, GRID_HEIGHT,
                    "resources/list.lst");

            modelsimPane1 = modelSimScene1.getPane();
        } catch (IOException e) {
            modelsimPane1 = defaultPane();
            modelSimScene1 = null;
            System.out.println("Modelsim file not found");
        }
        modelSimScene = modelSimScene1;
        modelsimPane = modelsimPane1;
    }


    public static void main(String[] args) {
        launch(args);
    }

    private Pane defaultPane() {
        HBox temp = new HBox();
        Text text = new Text("File not found");
        text.setLayoutX(APP_WIDTH/2d);
        text.setLayoutY(200);
        text.setStyle("-fx-font-size: 50px; -fx-font-weight: bold");

        temp.setPrefSize(APP_WIDTH, APP_HEIGHT);

        temp.getChildren().add(text);
        temp.setAlignment(Pos.CENTER);
        return temp;
    }


    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Breakout-sim");
        primaryStage.setResizable(false);

        Button swapBtn = getButton(J_STR);
        swapBtn.setOnAction(event -> {
            if (swapBtn.getText().equals(J_STR)) {
                swapBtn.setText(M_STR);
                modelsimPane.setVisible(false);
                jenkinsPane.setVisible(true);
            } else if (swapBtn.getText().equals(M_STR)) {
                swapBtn.setText(J_STR);
                modelsimPane.setVisible(true);
                jenkinsPane.setVisible(false);
            }
            swapBtn.toFront();
        });

        Group group = new Group(swapBtn, modelsimPane, jenkinsPane);

        Scene scn = new Scene(group, APP_WIDTH, APP_HEIGHT);
        scn.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            switch (event.getCode()) {
                case LEFT -> {
                    if (jenkinsScene != null)
                        jenkinsScene.getTimebar().previous();
                    if (modelSimScene != null)
                        modelSimScene.getTimebar().previous();
                }
                case RIGHT -> {
                    if (jenkinsScene != null)
                        jenkinsScene.getTimebar().next();
                    if (modelSimScene != null)
                        modelSimScene.getTimebar().next();
                }
            }
            event.consume();
        });

        jenkinsPane.setVisible(false);
        swapBtn.toFront();


        primaryStage.setScene(scn);
        primaryStage.show();

    }


    private Button getButton(String text) {
        Button btn = new Button(text);
        btn.setStyle("-fx-font-size: 10px; -fx-text-alignment: left; -fx-font-weight: bold");

        btn.setLayoutX(10);
        btn.setLayoutY(390);

        return btn;
    }

    // VM options: --module-path /usr/share/openjfx/lib --add-modules=javafx.controls

}

