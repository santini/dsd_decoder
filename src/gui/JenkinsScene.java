package gui;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import utils.Parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class JenkinsScene {

    private static final Color MISSING_COLOR = Color.BLUE;
    private static final Color TO_MUCH_COLOR = Color.RED;

    private static final int OFF_SET_X = 75;
    private static final int OFF_SET_Y = 50;


    private final int gridWidth, gridHeight;

    private final List<boolean[][]> goldenArrays;
    private final List<boolean[][]> buildArrays;

    private final List<Integer> undefinedFramesGold = new ArrayList<>();
    private final List<Integer> undefinedFramesBuild = new ArrayList<>();

    private final Map<Integer, String> inputs;

    private final TimeBar timebar;

    private final ObjectProperty<boolean[][]> goldenTab = new SimpleObjectProperty<>();
    private final ObjectProperty<boolean[][]> buildTab = new SimpleObjectProperty<>();

    private final BooleanProperty errorFrameGold = new SimpleBooleanProperty();
    private final BooleanProperty errorFrameBuild = new SimpleBooleanProperty();

    private final BorderPane pane = new BorderPane();

    private final Pane golden = new Pane();
    private final Pane build = new Pane();


    JenkinsScene(int gridWidth, int gridHeight, String gold, String build) throws IOException {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        Parser parser = new Parser(gridWidth, gridHeight);

        this.goldenArrays = parser.parseDisplay(gold, undefinedFramesGold);
        this.buildArrays = parser.parseDisplay(build, undefinedFramesBuild);


        undefinedFramesGold.forEach(id -> goldenArrays.add(id, null));
        undefinedFramesBuild.forEach(id -> buildArrays.add(id, null));

        this.timebar = new TimeBar(Math.max(goldenArrays.size(), buildArrays.size()) - 1);
        this.inputs = parser.parseInputs(build);

        errorFrameGold.bind(timebar.index().map(id -> undefinedFramesGold.contains(id.intValue())));
        errorFrameBuild.bind(timebar.index().map(id -> undefinedFramesBuild.contains(id.intValue())));


        goldenTab.bind(timebar.index().map(id -> goldenArrays.get(id.intValue())));
        buildTab.bind(timebar.index().map(id -> buildArrays.get(id.intValue())));

        initPanes();

    }


    private void initPanes() {

        golden.setStyle("-fx-background-color: #efa810");
        golden.setPrefWidth(400);
        build.setPrefWidth(400);

        pane.setLeft(golden);
        pane.setRight(build);

        // Setting labels
        Label goldLabel = new Label("Golden");
        Label buildLabel = new Label("Build");

        goldLabel.setLayoutX(5);
        goldLabel.setLayoutY(320);
        goldLabel.setStyle("-fx-font-size: 25px; -fx-font-weight: bold");

        buildLabel.setLayoutX(5);
        buildLabel.setLayoutY(320);
        buildLabel.setStyle("-fx-font-size: 25px; -fx-font-weight: bold");

        golden.getChildren().add(goldLabel);
        build.getChildren().add(buildLabel);

        pane.setTop(timebar);

        Text errorText = new Text("Undefined of 'X' bit detected on this frame");
        errorText.setStyle("-fx-font-size: 15px; -fx-font-weight: bold");
        errorText.setLayoutX(App.APP_WIDTH/2d - 150);
        errorText.setLayoutY(App.APP_HEIGHT - 5);

        errorText.visibleProperty().bind(errorFrameGold.or(errorFrameBuild));

        pane.getChildren().add(errorText);

        setGridGroup();
        setInputs(golden);
        setLegend(build);


    }



    private void setInputs(Pane pane) {
        Text text = new Text();
        text.setLayoutX(5);
        text.setLayoutY(20);

        text.setStyle("-fx-font-size: 20px; -fx-font-weight: bold");


        timebar.index().addListener((e, oldV, newV) -> {
            if (inputs.containsKey(newV.intValue())) {
                text.setText(inputs.get(newV.intValue()));
                text.setVisible(true);
            } else {
                text.setVisible(false);
            }
        });

        pane.getChildren().add(text);

    }


    private void setLegend(Pane pane) {
        BorderPane legend = new BorderPane();
        legend.setStyle("-fx-font-size: 10px; -fx-text-alignment: left; -fx-font-weight: bold");

        legend.setTop(getLine("Should be off", Color.RED));
        legend.setBottom(getLine("Should be on", Color.BLUE));

        legend.setLayoutX(300);
        legend.setLayoutY(300);

        pane.getChildren().add(legend);

    }

    private Pane getLine(String txt, Color color) {
        BorderPane line = new BorderPane();

        Rectangle surplusRect = new Rectangle(20, 20, color);
        surplusRect.setArcHeight(10);
        surplusRect.setArcWidth(10);
        line.setLeft(surplusRect);

        line.setRight(new Text(txt));

        return line;
    }



    private void setGridGroup() {

        for (int i = 0; i < gridWidth; i++) {
            for (int j = 0; j < gridHeight; j++) {
                final int finalI = i; // Need values final for lambda
                final int finalJ = j;

                Rectangle buildRect = new Rectangle(22 * i + OFF_SET_X, 22 * j + OFF_SET_Y, 20, 20);
                Rectangle goldRect = new Rectangle(22 * i + OFF_SET_X, 22 * j + OFF_SET_Y, 20, 20);

                goldRect.visibleProperty().bind(goldenTab.map(goldTab -> goldTab != null && goldTab[finalI][finalJ]));
                goldRect.setFill(Color.BLACK);
                goldRect.setArcWidth(15);
                goldRect.setArcHeight(15);

                golden.getChildren().add(goldRect);


                buildRect.visibleProperty().bind(Bindings.createBooleanBinding(
                        () ->   goldenTab.get() != null && buildTab.get() != null &&
                                (goldenTab.get()[finalI][finalJ] || buildTab.get()[finalI][finalJ]),
                        goldenTab, buildTab));

                buildRect.fillProperty().bind(Bindings.createObjectBinding(
                        () -> {
                            if (goldenTab.get() == null || buildTab.get() == null)
                                return Color.TRANSPARENT;
                            boolean goldV = goldenTab.get()[finalI][finalJ];
                            boolean buildV = buildTab.get()[finalI][finalJ];
                            if (!goldV && buildV) return TO_MUCH_COLOR;
                            else if (goldV && !buildV) return MISSING_COLOR;
                            else return Color.BLACK;
                        },
                        goldenTab, buildTab));

                buildRect.setArcWidth(15);
                buildRect.setArcHeight(15);

                build.getChildren().add(buildRect);


            }
        }
    }


    Pane getPane() { return pane; }

    TimeBar getTimebar() { return timebar; }


}
