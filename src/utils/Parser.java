package utils;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public final class Parser {

    private final int width;
    private final int height;

    private final static Pattern PATTERN_OUT = Pattern.compile("^((# )?\\d+:)");
    private final static Pattern HEX_PATTERN = Pattern.compile("(# )?\\d+:[0-9a-fA-F]+");
    private final static Pattern PATTERN_IN = Pattern.compile("^(# \\d+->)");
    private final static Pattern MODELSIM_PATTERN = Pattern.compile("([0-1]+  \\+[0-1]+ [0-1]+( )?)$");

    public Parser(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public List<boolean[][]> parseDisplay(String path, List<Integer> errorFrames) throws IOException {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(path), UTF_8))) {

            return  br.lines()
                    .filter(str -> PATTERN_OUT.matcher(str).lookingAt())
                    .peek(str -> {
                        if (!HEX_PATTERN.matcher(str).matches()) errorFrames.add(Integer.parseInt(str.split(":")[0]
                                .replace("# ", "")));})
                    .filter(str -> HEX_PATTERN.matcher(str).matches())
                    .map(this::toBoolArray)
                    .collect(Collectors.toList());
        }
    }

    public Map<Integer, String> parseInputs(String path) throws IOException {
        Map<Integer, String> inputs = new HashMap<>();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(path), UTF_8))) {

            br.lines().filter(str -> PATTERN_IN.matcher(str).lookingAt())
                    .forEach(line -> {
                        String[] split = line.split("->");
                        Integer index = Integer.parseInt(split[0].replace("# ", ""));
                        inputs.put(index, split[1]);
                    });
        }
        return inputs;
    }


    public List<boolean[][]> parseModelsimTest(String path) throws IOException {

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(path), UTF_8))) {

            return br.lines()
                    .map(l -> l.replaceAll("\\s", ""))
                    .filter(l -> l.length() > 100)
                    .map(l -> l.substring(l.length() - 108))
                    .map(this::transformStringToBooleanArray)
                    .collect(Collectors.toList());
        }


    }

    private boolean[][] transformStringToBooleanArray(String s) {
        boolean[][] buffer = new boolean[width][height];
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                buffer[i % 12][i / 12] = true;
            }
        }
        return buffer;
    }


    private boolean[][] toBoolArray(String line) {
        boolean[][] buffer = new boolean[width][height];

        String s = line.split(":")[1];

        byte[] bytes = HexFormat.of().parseHex(s.length() % 2 == 1 ? s + "0" : s);

        for (int j = 0; j < height; ++j) {
            for (int i = 0; i < width; ++i) {
                int index = i + j * width;
                int b = bytes[index / Byte.SIZE] >>> (Byte.SIZE - (index % Byte.SIZE) - 1);
                buffer[i][j] = (b & 1)  == 1;
            }
        }

        return buffer;
    }


}
